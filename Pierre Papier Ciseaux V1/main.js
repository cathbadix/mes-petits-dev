document.addEventListener('DOMContentLoaded', () => {
    const buttons = document.querySelectorAll("button");

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function() {
            const joueur = buttons[i].innerHTML;
            const robot = buttons[Math.floor(Math.random() * buttons.length)].innerHTML;
            let resultat = "";

            if (joueur === robot) {
                resultat = "Egalité";
            }
            else if ((joueur === "Pierre" && robot === "Ciseaux") || 
                    (joueur === "Papier" && robot === "Pierre") || 
                    (joueur === "Ciseaux" && robot === "Papier")) {
                        resultat = "Gagné";
                    }
                    else {
                        resultat = "Perdu";
                    }            
            document.querySelector(".block-resultat").innerHTML = `Joueur : ${joueur} Robot: ${robot} ${resultat}`            
        });
    }
})