# Once upon a time

## 1/ Personnage Joueur:

### War "nom"
Stats: (force:); (endurance:); (critique:); (dégât des coups critiques:);

Spells: (fendoir:); (martelet:); (brise genoux:); (désarmer:);

Weapon:

### Priest "nom"  
Stats: (intelligence:); (endurance:); (critique:); (dégât des coups critiques:);

Spells:(marteau sacré:); (soin:); (bouclier:); (résurection:);

Weapon:

### Rogue "nom" 
Stats: (dextérité:); (endurance:); (critique:); (dégât des coups critiques:);

Spells: (suriner:); (frappes multiples:); (Marque:); (Evitation:);

Weapon:


## 2/ Personnage IA:

### Gobelin:
Stats: (force:); (endurance:); (critique:); (dégât des coups critiques:); 

Spells: (sabordé:); (double frappes:);  

Weapon:

### Mandragore:
Stats: (dexterité:); (endurance:) (critique:); (dégât des coups critiques:); 

Spells: (fouet:); (laceret:);

Weapon:

### Arcanist:
Stats: (intelligence:); (endurance:); (critique:); (dégât des coups critiques:);

Spells: (déflagration:); (boule de feu:); 

Weapon:


## 3/ Inventaire:

